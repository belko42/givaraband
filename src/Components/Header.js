import React, { Component } from 'react'
import {
    Container,
    Navbar,
    Nav,
    FormControl,
    Form,
    Button
} from 'react-bootstrap'
import logo from './logo192.png'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import Home from '../Pages/Home'
import About from '../Pages/About'
import Contact from '../Pages/Contact'
import Test from '../Pages/Test'

export default class Header extends Component {
    render() {
        return (
            <>
                <Navbar collapseOnSelect expand="md" bg="dark" variant="dark">
                    <Container>
                        <Navbar.Brand href="/">
                            <img
                                src={logo}
                                height="30"
                                width="30"
                                className="d-inline-block align-top"
                                alt="logo"
                            />
                        </Navbar.Brand>
                        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                        <Navbar.Collapse id="responsive-navbar-nav">
                            <Nav className="mr-auto">
                                <Nav.Link href="/"> Home </Nav.Link>
                                <Nav.Link href="/about"> About </Nav.Link>
                                <Nav.Link href="/contact"> Contacts </Nav.Link>
                                <Nav.Link href="/test"> Test </Nav.Link>
                            </Nav>
                            <Form
                                className="d-flex"
                            >
                                <FormControl
                                    type="text"
                                    placeholder="Search"
                                    className="mr-2"
                                />
                                <Button variant="outline-info">Search</Button>
                            </Form>
                        </Navbar.Collapse>
                    </Container>
                </Navbar>

                <Router>
                    <Switch>
                        <Route exact path="/" component={Home} />
                        <Route exact path="/about" component={About} />
                        <Route exact path="/contact" component={Contact} />
                        <Route exact path="/test" component={Test} />
                    </Switch>
                </Router>
            </>
        )
    }
}